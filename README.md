# Aplicación FooApp
## Nativescript con Angular
## Contenido:
- [Componentes de interface nativescript](https://docs.nativescript.org/angular/start/introduction)
- [RadListView](https://docs.nativescript.org/angular/ui/ng-components/ng-RadListView/getting-started#radlistview-for-angular---getting-started) 
- [Sass](https://docs.nativescript.org/ui/styling#using-sass)
- Plugins:
    - [MDCardView](https://github.com/Akylas/nativescript-material-components/blob/master/packages/nativescript-material-cardview/README.md)
    - [Mapbox](https://market.nativescript.org/plugins/nativescript-mapbox)
    - [LocalStorage](https://market.nativescript.org/plugins/nativescript-localstorage)
    - [Firebase](https://github.com/EddyVerbruggen/nativescript-plugin-firebase)
- Uso de apis:
    - [Zomato Api](https://developers.zomato.com/api?lang=es_cl)
    - [REQ | RES](https://reqres.in)

### Para usar el proyecto ejecutar los siguientes comandos:

```
    npm install
    tns run android
    tns run ios
```
