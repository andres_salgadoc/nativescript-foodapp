import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { Page } from 'tns-core-modules/ui/page/page';
import { AuthService } from '../services/auth.service';
import * as firebase from 'nativescript-plugin-firebase';

@Component({
  selector: 'ns-register',
  providers:[AuthService],
  templateUrl: './register.component.html'
})
export class RegisterComponent implements OnInit {

  public email: string = 'eve.holt@reqres.in';
  public password: string = 'pistol';

  constructor(private page: Page, private authService:AuthService, private router: RouterExtensions) { 
    this.page.actionBarHidden = true;
  }
  
  ngOnInit() {
  }

  public onRegister() {
    this.authService.register( this.email, this.password ).subscribe( res => {
      this.authService.setUser( 'food-app',res, this.email , this.email );
    	this.router.navigate( ['/home/menu' ], { transition: { name: 'slide' }, clearHistory: true});
    }, error => console.log(error) );
  }

  public onLoginFacebook() {
    firebase.login({
      type: firebase.LoginType.FACEBOOK,
      facebookOptions: {
        scopes: [ 'public_profile', 'email' ]
      }
    }).then(res => {
      this.authService.setUser('facebook',res,res.email,res.displayName);
      this.router.navigate( [ '/home/menu' ], { transition: { name: 'slide'} , clearHistory: true } );
    }, error => console.log(error));
  }

  public onLoginGoogle() {
    firebase.login({
      type: firebase.LoginType.GOOGLE,
      // Optional 
      googleOptions: {
        hostedDomain: "mygsuitedomain.com",
        // NOTE: no need to add 'profile' nor 'email', because they are always provided
        // NOTE 2: requesting scopes means you may access those properties, but they are not automatically fetched by the plugin
        scopes: ['https://www.googleapis.com/auth/user.birthday.read']
      }
    }).then(result => {
      this.authService.setUser('google',result,result.email,result.displayName);
      this.router.navigate( [ '/home/menu' ], { transition: { name: 'slide'} , clearHistory: true } );
      }, errorMessage => console.log( errorMessage ) );
  }
  
}
