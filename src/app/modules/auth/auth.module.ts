import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { Routes } from '@angular/router';
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NativeScriptRouterModule } from 'nativescript-angular/router';
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from '../../auth.guard';
import { AuthService } from './services/auth.service';

const routes: Routes = [
  { path:'', canActivate: [AuthGuard], component: LoginComponent },
  { path:'register',component: RegisterComponent }
]

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forChild(routes)
  ],
providers: [
    AuthGuard,
    AuthService
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AuthModule { }
