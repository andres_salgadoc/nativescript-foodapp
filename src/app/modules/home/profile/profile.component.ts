import { Component, OnInit } from '@angular/core';
import { RouterExtensions } from 'nativescript-angular/router';
import { AuthService } from '../../auth/services/auth.service';
import * as firebase from 'nativescript-plugin-firebase';


@Component({
  selector: 'ns-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  user:any;

  constructor( private router: RouterExtensions, private authS: AuthService ) { }

  ngOnInit() {
    this.user = this.authS.currentUser().user;
    console.log(this.user);
  }

  public onLogout() {
    let userType = this.authS.currentUser().type;
    if( userType === 'google' || userType === 'facebook' ) {
      firebase.logout();
    }
    this.authS.deleteUser();
    this.router.navigate( [ '/' ], { transition: { name: 'slide' }, clearHistory: true });
  }

}
