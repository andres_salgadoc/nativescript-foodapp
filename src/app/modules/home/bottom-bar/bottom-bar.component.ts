import { Component, OnInit } from '@angular/core';
import { Page } from 'tns-core-modules/ui/page/page';

@Component({
  selector: 'ns-bottom-bar',
  templateUrl: './bottom-bar.component.html'
})
export class BottomBarComponent implements OnInit {

  constructor(private page:Page) {
    this.page.actionBarHidden = true;
   }

  ngOnInit() {
  }

}
