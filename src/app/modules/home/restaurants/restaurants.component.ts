import { Component, OnInit } from '@angular/core';
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';
import { RestaurantsService } from './services/restaurants.service';
import {TextField} from 'tns-core-modules/ui/text-field';
import { RouterExtensions } from 'nativescript-angular/router';

@Component({
  selector: 'ns-restaurants',
  providers:[RestaurantsService],
  templateUrl: './restaurants.component.html'
})
export class RestaurantsComponent implements OnInit {

  data: ObservableArray<any>;

  constructor(private rs:RestaurantsService, private router: RouterExtensions) { 
    this.getRestaurants();
  }

  ngOnInit() {
    
  }

  public getRestaurants(query: string = '') {
    this.data = new ObservableArray<any>([]);
    this.rs.search(query).subscribe((res:any) => {
      this.data.push(res.restaurants);
    }, error => console.log(error));
  }

  public onReturnPress(arg) {
    let textField = <TextField>arg.object;
    this.getRestaurants(textField.text);
   }

   public onNavigate(item:any) {
      this.router.navigate(['/home/restaurant-detail'],
      {
        transition: {name:'slide'},
        queryParams: {
          restaurant: JSON.stringify(item)
        }
      });
   }

}
