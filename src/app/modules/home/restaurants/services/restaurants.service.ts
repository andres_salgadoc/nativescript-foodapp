import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RestaurantsService {

  constructor(private http: HttpClient) { }
  
  public search( query: string = '' ) {
    return this.http.get(`https://developers.zomato.com/api/v2.1/search?count=10&q=${query.replace(/ /g,"%20")}`,
    {headers: this.commonHeaders()}
    );
  }

  private commonHeaders() {
    return new HttpHeaders({
      'user-key': '33f3a248e55c80f1357e2fea928f45ab',
    })
  }

}
