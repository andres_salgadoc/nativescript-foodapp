import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from './modules/auth/services/auth.service';
import { RouterExtensions } from 'nativescript-angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  
  constructor( private auth: AuthService, private router: RouterExtensions ) {}

  canActivate(): boolean {

    if( this.auth.currentUser() === null ) {
      return true;
    }
    this.router.navigate( [ '/home/menu' ], { transition: {name: 'slide'} , clearHistory:true } );
    return false;

  }
  
}
