import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";



const routes: Routes = [
    {path:'', loadChildren:() => import( './modules/auth/auth.module' ).then( m => m.AuthModule )},
    {path:'home', loadChildren:() => import( './modules/home/home.module' ).then( m => m.HomeModule )}
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
